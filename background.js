// Dev instructions
// go into chrome://extensions/ and enable "Developer mode"
// then click "LOAD UNPACKED" and select this directory.
// You can look for this extension and enable/refresh it.
//
// You can find tabs documentation at https://developer.chrome.com/extensions/tabs#method-update
// You can only mute through tabs#update method.

var tabCheckInterval = 1000; // check every second
var tabReportInterval = 30 * 6000; // report every 30 minutes; TODO: create interface

var lastTabId = 0;
var checkTabs = {};
var tabsAudible = {};
var reportAudible = {};

chrome.tabs.onSelectionChanged.addListener(function(tabId) {
  lastTabId = tabId;
  chrome.pageAction.show(lastTabId);
});

chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
  lastTabId = tabs[0].id;
  //chrome.pageAction.show(lastTabId);
});

// Called when the user clicks on the page action.
chrome.pageAction.onClicked.addListener(function(tab) {
  if (tab.id in checkTabs) {
    // unmute
    chrome.tabs.update(tab.id, { muted: false })
    console.log("unmuted " + tab.id)
    window.clearInterval(checkTabs[tab.id]);
    window.clearInterval(reportAudible[tab.id]);
    delete checkTabs[tab.id];
    delete reportAudible[tab.id];
    asyncLoadImageData("if_Audio_-_Video_-_Game_23_2298364.png", function(imageData) {
      chrome.pageAction.setIcon({ imageData: imageData, tabId: tab.id });
    });

  } else {
    chrome.tabs.update(tab.id, { muted: true })
    console.log("muted " + tab.id)
    asyncLoadImageData("if_Audio_-_Video_-_Game_24_2298386.png", function(imageData) {
      chrome.pageAction.setIcon({ imageData: imageData, tabId: tab.id });
    });

    var tabId = tab.id; // variable used in callbacks via closures

    // check if tab is making noise
    checkTabs[tab.id] = window.setInterval(function() {
      chrome.tabs.get(tabId, function(tab) {
        tabsAudible[tab.id] = tabsAudible[tab.id] || tab.audible;
        if (tab.audible) {
          var msg = "tab: " + tab.id + " muted:" + tab.mutedInfo.muted + " audible: " + tab.audible;
          console.log(msg);
        }
      });
    }, tabCheckInterval);

    // play sound if tab made noise in the tabReportInterval
    reportAudible[tab.id] = window.setInterval(function() {
      console.log("tab: " + tabId + " audible in period: " + tabsAudible[tabId]);

      // play notification once period is over
      if (tabsAudible[tabId]) {
        var audio = new Audio("sms-alert-5-daniel_simon.mp3"); // from http://soundbible.com/2158-Text-Message-Alert-5.html
        audio.play();
      }

      tabsAudible[tabId] = false;
    }, tabReportInterval);
  }
});

// mute tab
// inspiration from source: https://github.com/jaredsohn/mutetab/blob/master/src/js/background/chrome_misc.js
function toggleMute(tab) {
  if ((typeof tab === "undefined") || (tab === null)) {
    return;
  }

  if ((!tab.hasOwnProperty("mutedInfo")) && (tab.hasOwnProperty("mutedCause"))) {
    tab.mutedInfo = {};
    tab.mutedInfo.muted = tab.muted || false;
    var reasons = ["capture", "user", ""];
    if (reasons.indexOf(tab.mutedCause) >= 0) {
      tab.mutedInfo.reason = tab.mutedCause;
    } else {
      tab.mutedInfo.reason = "extension";
      tab.mutedInfo.extensionId = tab.mutedCause || "";
    }
  }


}

// HACK: create canvas since SetIcon doesn't work with path.
// source: https://stackoverflow.com/a/28765872/21908
function asyncLoadImageData(path, callback) {
  var canvas = document.createElement("canvas");
  var ctx = canvas.getContext("2d");
  var image = new Image();
  image.onload = function() {
    ctx.drawImage(image,0,0,19,19);
    var imageData = ctx.getImageData(0,0,19,19);
    callback(imageData);
  }
  image.src = chrome.runtime.getURL(path);
}

chrome.runtime.onInstalled.addListener(function() {
  // chrome.storage.sync.set({color: '#3aa757'}, function() {
  //   console.log("The color is green.");
  // });

  chrome.declarativeContent.onPageChanged.removeRules(undefined, function() {
    asyncLoadImageData("if_Audio_-_Video_-_Game_23_2298364.png", function(imageData) {
      chrome.declarativeContent.onPageChanged.addRules([
        {
          conditions: [new chrome.declarativeContent.PageStateMatcher({
            pageUrl: { schemes: ["http", "https"] },
          })],
          actions: [
            new chrome.declarativeContent.ShowPageAction(),
            new chrome.declarativeContent.SetIcon({ imageData: imageData })
          ]
        }
      ]);
    })
  });
});
